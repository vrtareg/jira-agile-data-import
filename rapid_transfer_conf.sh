# Configuration Script for the Copy script
# Do not make executable !!!


# Destination Database Name - need to be separate !
MYSQL_DST_DB="dst_jira_db_name"

SED_SPRINT_ID="rapid_transfer_spID.sed"

# Any User ID changes if so
SED_USERS_ID="rapid_transfer_userID.sed"

# Rank Field ID's
CF_RANK_NAME="Rank"
CF_RANK_SRCID=<ID in Source Jira>
CF_RANK_DSTID=<ID in Destination Jira>

IND=0
CF_LIST_NAME[${IND}]=${CF_RANK_NAME}
CF_LIST_SRCID[${IND}]=${CF_RANK_SRCID}
CF_LIST_DSTID[${IND}]=${CF_RANK_DSTID}

# Story Points Field ID's
CF_SPOINTS_NAME="Story Points"
CF_SPOINTS_SRCID=<ID in Source Jira>
CF_SPOINTS_DSTID=<ID in Destination Jira>

IND=$((IND+1))
CF_LIST_NAME[${IND}]=${CF_SPOINTS_NAME}
CF_LIST_SRCID[${IND}]=${CF_SPOINTS_SRCID}
CF_LIST_DSTID[${IND}]=${CF_SPOINTS_DSTID}

unset IND


# Sprint Field ID's
CF_SPRINT_NAME="Sprint"
CF_SPRINT_SRCID=<ID in Source Jira>
CF_SPRINT_DSTID=<ID in Destination Jira>


# Agile Boards to Migrate

#First Board
IND=0
RAPID_PRKEY[${IND}]="<Board Project Keys in format 'KEY1', 'KEY2', .. and so on>"
RAPID_SRCID[${IND}]="<Board ID in Source Jira>"
RAPID_BNAME[${IND}]="<Board Name>"
RAPID_DSTID[${IND}]="<Board ID in Destination Jira>"
RAPID_SCRUM[${IND}]="<YES for Scrum, NO for Kanban>"

#Next Board, repeat this section for the rest of boards
IND=$((IND+1))
RAPID_PRKEY[${IND}]="<Board Project Keys in format 'KEY1', 'KEY2', .. and so on>"
RAPID_SRCID[${IND}]="<Board ID in Source Jira>"
RAPID_BNAME[${IND}]="<Board Name>"
RAPID_DSTID[${IND}]="<Board ID in Destination Jira>"
RAPID_SCRUM[${IND}]="<YES for Scrum, NO for Kanban>"



unset IND

# Statuses list if the ID's are different in Source and destintion
# First Status
IND=1
STATUS_NAME[${IND}]="<Status Name - in Like style with %>"
STATUS_OLDID[${IND}]="<Status ID in Source Jira>"
STATUS_NEWID[${IND}]="<Status ID in Destination Jira>"

# Second and so on statues if so
IND=$((IND+1))
STATUS_NAME[${IND}]="<Status Name - in Like style with %>"
STATUS_OLDID[${IND}]="<Status ID in Source Jira>"
STATUS_NEWID[${IND}]="<Status ID in Destination Jira>"

unset IND

