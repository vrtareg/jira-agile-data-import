# README #

### This Repository is designed for the Agile Boards Migration script ###


Jira Project Export/Import procedure does not migrate Agile Boards and data associated with it.

This script is designed to do that with some manual steps.

The process of migration:

* Prepare Source Jira with standardising and cleaning up the configuration
* Copy the configuration over using some Add-On - like [Project Configurator for JIRA](https://marketplace.atlassian.com/plugins/com.awnaba.projectconfigurator.projectconfigurator/server/overview)
* Create filters which will be used in Boards
* Create Boards with same names
* Record the Board Name, ID and Type in rapid_transfer_conf.sh file together with other configuration option
* Configure Database settings for MySQL for example in **Source** `rapid_transfer_my_src.cnf` and **Destination** `rapid_transfer_my_dst.cnf` DB config files. Prefferably Source user should be read only one
* Do the Projects Import using Jira Functionality
* Using Jira [LexoRank management](https://confluence.atlassian.com/display/JIRAKB/LexoRank+Management) tool be sure that both of the instances storing Rank in the same bucket. If no, rebalance one of the systems to make a match. It will be easier to do the less issues containing one.
* Shut both of the Jira instances
* Backup DB's
* Run `rapid_transfer_copy.sh` script
+ Before running jira back check rank field for any duplicates:
    * SQL for check
> SELECT rank, COUNT(rank)
> FROM AO_60DB71_LEXORANK
> GROUP BY rank
> HAVING COUNT(rank) > 1
    * For any duplicate RANK field value find the nearest values in database and try to change duplicate RANK field by adding numbers or letters to the end before ":" to shift it one position up or down and still avoid duplicate with other values.
* Start JIRA, run **JIRA Health-check** and **Lexorank Integrity Checks**