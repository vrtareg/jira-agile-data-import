#!/bin/bash

if [ -f rapid_transfer_conf.sh ]; then
	. rapid_transfer_conf.sh
else
	echo "Config file rapid_transfer_conf.sh does not exist"
	exit 0
fi

MYSQL_SRC_SEL="mysql --defaults-extra-file=rapid_transfer_my_src.cnf --skip-column-names --delimiter=';' "

# Destination Database name should be in the command line and not in config file
MYSQL_DST_INS="mysql --defaults-extra-file=rapid_transfer_my_dst.cnf ${MYSQL_DST_DB} "
MYSQL_DST_SEL="mysql --defaults-extra-file=rapid_transfer_my_dst.cnf --skip-column-names --delimiter=';' ${MYSQL_DST_DB} "


>${SED_SPRINT_ID}

show_sql_on_error() {

	if [ ${RET} -gt 0 ]; then
		echo ${1}
		read -p "Press Enter to continue or Ctrl + C to terminate"
	fi

}

for ind in ${!RAPID_SRCID[@]}; do

	echo ${RAPID_PRKEY[${ind}]}
	echo ${RAPID_SRCID[${ind}]}
	echo ${RAPID_BNAME[${ind}]}
	echo ${RAPID_DSTID[${ind}]}
	echo ${RAPID_SCRUM[${ind}]}

	echo "Processing Board ${RAPID_BNAME[${ind}]} for Project ${RAPID_PRKEY[${ind}]} (Source ID ${RAPID_SRCID[${ind}]}, Destination ID ${RAPID_DSTID[${ind}]}, Scrum ${RAPID_SCRUM[${ind}]})"

	if [ ${RAPID_SCRUM[${ind}]} = "YES" ]; then

		# Selecting sprints for the Project Boards - Boards should be created on Destination Jira and Project data should be imported before running this script
		SQL_SPRINTS="select CLOSED, COMPLETE_DATE, END_DATE, NAME, RAPID_VIEW_ID, SEQUENCE, STARTED, START_DATE, ID from AO_60DB71_SPRINT where RAPID_VIEW_ID = ${RAPID_SRCID[${ind}]}"
		${MYSQL_SRC_SEL} -e "${SQL_SPRINTS}" > /dev/null
		RET=$?
		show_sql_on_error "${SQL_SPRINTS}"

		for SPRINT in $( ${MYSQL_SRC_SEL} -e "${SQL_SPRINTS}" | sed -e 's/ /~~~/g; s/\t/@@@/g' ); do

			SPRINT_F=$( echo ${SPRINT} | sed -e 's/~~~/ /g; s/@@@/;/g' )
			SPRINT_CLOSED=$( echo ${SPRINT_F} | awk -F ';' '{print $1}' )
			SPRINT_COMPLETE_DATE=$( echo ${SPRINT_F} | awk -F ';' '{print $2}' )
			SPRINT_END_DATE=$( echo ${SPRINT_F} | awk -F ';' '{print $3}' )
			SPRINT_NAME="$( echo ${SPRINT_F} | awk -F ';' '{print $4}' )"
			# New Board ID's needs to get from the new Jira and put to the script
			#SPRINT_RAPID_VIEW_ID=$( echo ${SPRINT_F} | awk '{print $5}' | sed -f  ${SED_RAPID_VIEW_ID} )
			SPRINT_SEQUENCE=$( echo ${SPRINT_F} | awk -F ';' '{print $6}' )
			SPRINT_STARTED=$( echo ${SPRINT_F} | awk -F ';' '{print $7}' )
			SPRINT_START_DATE=$( echo ${SPRINT_F} | awk -F ';' '{print $8}' )
			SPRINT_SRCID=$( echo ${SPRINT_F} | awk -F ';' '{print $9}' )

			echo "    Copying Sprint: ${SPRINT_NAME}"

			SQL_SPRINT_INS="insert into AO_60DB71_SPRINT(CLOSED, COMPLETE_DATE, END_DATE, NAME, RAPID_VIEW_ID, SEQUENCE, STARTED, START_DATE) values (${SPRINT_CLOSED}, ${SPRINT_COMPLETE_DATE}, ${SPRINT_END_DATE}, '${SPRINT_NAME}', ${RAPID_DSTID[${ind}]}, ${SPRINT_SEQUENCE}, ${SPRINT_STARTED}, ${SPRINT_START_DATE})"

			#echo ${SQL_SPRINT_INS}
			#read
			${MYSQL_DST_INS} -e "${SQL_SPRINT_INS}"
			RET=$?
			show_sql_on_error "${SQL_SPRINT_INS}"

			SQL_SPRINT_DSTID="select max(ID) from AO_60DB71_SPRINT"
			SPRINT_DSTID=$( ${MYSQL_DST_SEL} -e "${SQL_SPRINT_DSTID}" )
			RET=$?
			show_sql_on_error "${SQL_SPRINT_DSTID}"

			# Saving Sprints Old/New ID's for Sprints Change Item fix
			echo "s/\\b${SPRINT_SRCID}\\b/${SPRINT_DSTID}/" >> ${SED_SPRINT_ID}

			SQL_AUDIT="select CATEGORY, DATA, ENTITY_CLASS, ENTITY_ID, TIME, USER from AO_60DB71_AUDITENTRY where ENTITY_ID = ${SPRINT_SRCID}"
			${MYSQL_SRC_SEL} -e "${SQL_AUDIT}" > /dev/null
			RET=$?
			show_sql_on_error "${SQL_AUDIT}"

			for AUDIT in $( ${MYSQL_SRC_SEL} -e "${SQL_AUDIT}" | sed -e 's/ /@@@/g; s/\t/@@@/g' ); do

				AUDIT_F=$( echo ${AUDIT} | sed -e 's/@@@/ /g' )
				AUDIT_CATEGORY=$( echo ${AUDIT_F} | awk '{print $1}' )
				AUDIT_DATA=$( echo ${AUDIT_F} | awk '{print $2}' | sed -e 's/"/""/g' )
				AUDIT_ENTITY_CLASS="$( echo ${AUDIT_F} | awk '{print $3}' )"
				AUDIT_ENTITY_ID=$( echo ${AUDIT_F} | awk '{print $4}' )
				AUDIT_TIME=$( echo ${AUDIT_F} | awk '{print $5}' )
				AUDIT_USER=$( echo ${AUDIT_F} | awk '{print $6}' | sed -f ${SED_USERS_ID} )

				echo "    Copying Audit Entry: ${AUDIT_DATA}"

				SQL_AUDIT_INS="insert into AO_60DB71_AUDITENTRY(CATEGORY, DATA, ENTITY_CLASS, ENTITY_ID, TIME, USER) values (\"${AUDIT_CATEGORY}\", \"${AUDIT_DATA}\", \"${AUDIT_ENTITY_CLASS}\", ${SPRINT_DSTID}, ${AUDIT_TIME}, \"${AUDIT_USER}\")"
				#echo ${SQL_AUDIT_INS}
				#read
				${MYSQL_DST_INS} -e "${SQL_AUDIT_INS}"
				RET=$?
				show_sql_on_error "${SQL_AUDIT_INS}"
				#read

			done

			# Copying Sprint Custom Field Data Values
			SQL_CFVALUE="SELECT
    project.pkey
    , jiraissue.issuenum
    , customfieldvalue.CUSTOMFIELD
    , customfieldvalue.PARENTKEY
    , customfieldvalue.STRINGVALUE
    , customfieldvalue.NUMBERVALUE
    , customfieldvalue.TEXTVALUE
    , customfieldvalue.DATEVALUE
    , customfieldvalue.VALUETYPE
FROM
    project
    LEFT JOIN jiraissue 
        ON (project.ID = jiraissue.PROJECT)
    LEFT JOIN customfieldvalue 
        ON (jiraissue.ID = customfieldvalue.ISSUE)
WHERE (project.pkey in (${RAPID_PRKEY[${ind}]})
    AND customfieldvalue.CUSTOMFIELD = ${CF_SPRINT_SRCID}
    AND customfieldvalue.STRINGVALUE = ${SPRINT_SRCID});"

			${MYSQL_SRC_SEL} -e "${SQL_CFVALUE}" > /dev/null
			RET=$?
			show_sql_on_error "${SQL_CFVALUE}"

			for CFVALUE in $( ${MYSQL_SRC_SEL} -e "${SQL_CFVALUE}" | sed -e 's/ /@@@/g; s/\t/@@@/g'  ); do

				CFVALUE_F=$( echo ${CFVALUE}             | sed -e 's/@@@/ /g' )
				CFVALUE_PRKEY=$( echo ${CFVALUE_F}       | awk '{print $1}' )
				CFVALUE_ISSUENUM=$( echo ${CFVALUE_F}    | awk '{print $2}' )
				CFVALUE_CUSTOMFIELD=$( echo ${CFVALUE_F} | awk '{print $3}' )
				CFVALUE_PARENTKEY=$( echo ${CFVALUE_F}   | awk '{print $4}' )
				CFVALUE_STRINGVALUE=$( echo ${CFVALUE_F} | awk '{print $5}' )
				CFVALUE_NUMBERVALUE=$( echo ${CFVALUE_F} | awk '{print $6}' )
				CFVALUE_TEXTVALUE=$( echo ${CFVALUE_F}   | awk '{print $7}' )
				CFVALUE_DATEVALUE=$( echo ${CFVALUE_F}   | awk '{print $8}' )
				CFVALUE_VALUETYPE=$( echo ${CFVALUE_F}   | awk '{print $9}' )

				SQL_CFVALUE_ID="select max(id)+1 from customfieldvalue"
				CFVALUE_ID=$( ${MYSQL_DST_SEL} -e "${SQL_CFVALUE_ID}" )

				echo "    Copying custom Field Value for Issue ${CFVALUE_PRKEY}-${CFVALUE_ISSUENUM}, Sprint ${SPRINT_DSTID}"

				SQL_CFVALUE_INS="insert into customfieldvalue(ID, ISSUE, CUSTOMFIELD, STRINGVALUE) select ${CFVALUE_ID} as 'ID', jiraissue.id as 'ISSUE', ${CF_SPRINT_DSTID} as 'CUSTOMFIELD', ${SPRINT_DSTID} as 'STRINGVALUE' from jiraissue INNER JOIN project ON (jiraissue.PROJECT = project.ID) WHERE project.pkey = '${CFVALUE_PRKEY}' AND jiraissue.issuenum = ${CFVALUE_ISSUENUM}"

				${MYSQL_DST_INS} -e "${SQL_CFVALUE_INS}"
				RET=$?
				show_sql_on_error "${SQL_CFVALUE_INS}"

			done
		done
	fi


	# Copying quick filters
	SQL_QFILTER="SELECT DESCRIPTION, NAME, POS, QUERY
FROM AO_60DB71_QUICKFILTER
WHERE RAPID_VIEW_ID=${RAPID_SRCID[${ind}]}
  AND NAME NOT LIKE 'gh.%'"

	${MYSQL_SRC_SEL} -e "${SQL_QFILTER}" > /dev/null
	RET=$?
	show_sql_on_error "${SQL_QFILTER}"

	for QFILTER in $( ${MYSQL_SRC_SEL} -e "${SQL_QFILTER}" | sed -e 's/ /~~~/g; s/\t/@@@/g'  ); do

		QFILTER_F=$( echo ${QFILTER} | sed -e 's/~~~/ /g; s/@@@/;/g' )
		QFILTER_DESCRIPTION=$( echo ${QFILTER_F} | awk -F ';' '{print $1}' )
		QFILTER_NAME=$( echo ${QFILTER_F} | awk -F ';' '{print $2}' )
		QFILTER_POS=$( echo ${QFILTER_F} | awk -F ';' '{print $3}' )
		QFILTER_QUERY="$( echo ${QFILTER_F} | awk -F ';' '{print $4}' )"

		echo "    Copying Quick filter: ${QFILTER_NAME}"

		SQL_QFILTER_INS="insert into AO_60DB71_QUICKFILTER(DESCRIPTION, NAME, POS, QUERY, RAPID_VIEW_ID) values ('${QFILTER_DESCRIPTION}', '${QFILTER_NAME}', ${QFILTER_POS}, '${QFILTER_QUERY}', ${RAPID_DSTID[${ind}]})"

		#echo ${SQL_QFILTER_INS}
		${MYSQL_DST_INS} -e "${SQL_QFILTER_INS}"
		RET=$?
		show_sql_on_error "${SQL_QFILTER_INS}"

	done

	# Copying card colours
	SQL_CCOLOUR="SELECT COLOR, POS, STRATEGY, VAL
FROM AO_60DB71_CARDCOLOR
WHERE RAPID_VIEW_ID = ${RAPID_SRCID[${ind}]}
AND strategy <> 'issuetype'"

	${MYSQL_SRC_SEL} -e "${SQL_CCOLOUR}" > /dev/null
	RET=$?
	show_sql_on_error "${SQL_CCOLOUR}"

	for CCOLOUR in $( ${MYSQL_SRC_SEL} -e "${SQL_CCOLOUR}" | sed -e 's/ /~~~/g; s/\t/@@@/g'  ); do

		CCOLOUR_F=$( echo ${CCOLOUR} | sed -e 's/~~~/ /g; s/@@@/;/g' )
		CCOLOUR_COLOR="$( echo ${CCOLOUR_F} | awk -F ';' '{print $1}' )"
		CCOLOUR_POS=$( echo ${CCOLOUR_F} | awk -F ';' '{print $2}' )
		CCOLOUR_STRATEGY=$( echo ${CCOLOUR_F} | awk -F ';' '{print $3}' )
		CCOLOUR_VAL=$( echo ${CCOLOUR_F} | awk -F ';' '{print $4}' )
		if [ ${CCOLOUR_STRATEGY} = "assignee" ]; then
			CCOLOUR_VAL=$( echo ${CCOLOUR_VAL} | sed -f ${SED_USERS_ID} )
		fi

		echo "    Copying Card Colours: ${CCOLOUR_STRATEGY} - ${CCOLOUR_VAL}"

		SQL_CCOLOUR_INS="insert into AO_60DB71_CARDCOLOR(COLOR, POS, STRATEGY, VAL, RAPID_VIEW_ID) values ('${CCOLOUR_COLOR}', ${CCOLOUR_POS}, '${CCOLOUR_STRATEGY}', '${CCOLOUR_VAL}', ${RAPID_DSTID[${ind}]})"

		#echo ${SQL_CCOLOUR_INS}
		${MYSQL_DST_INS} -e "${SQL_CCOLOUR_INS}"
		RET=$?
		show_sql_on_error "${SQL_CCOLOUR_INS}"

	done

	# Copying SwimLanes
	SQL_SWLINES="SELECT DEFAULT_LANE, DESCRIPTION, NAME, POS, QUERY, LONG_QUERY
FROM AO_60DB71_SWIMLANE
WHERE RAPID_VIEW_ID=${RAPID_SRCID[${ind}]}
  AND NAME NOT LIKE 'gh.%'"
	${MYSQL_SRC_SEL} -e "${SQL_SWLINES}" > /dev/null
	RET=$?
	show_sql_on_error "${SQL_SWLINES}"

	for SWLINES in $( ${MYSQL_SRC_SEL} -e "${SQL_SWLINES}" | sed -e 's/ /~~~/g; s/\t/@@@/g'  ); do

		SWLINES_F=$(           echo ${SWLINES}   | sed -e 's/~~~/ /g; s/@@@/;/g' )
		SWLINES_LANE=$(        echo ${SWLINES_F} | awk -F ';' '{print $1}' )
		SWLINES_DESCRIPTION=$( echo ${SWLINES_F} | awk -F ';' '{print $2}' )
		SWLINES_NAME=$(        echo ${SWLINES_F} | awk -F ';' '{print $3}' )
		SWLINES_POS=$(         echo ${SWLINES_F} | awk -F ';' '{print $4}' )
		SWLINES_QUERY="$(      echo ${SWLINES_F} | awk -F ';' '{print $5}' )"
		SWLINES_LQUERY="$(     echo ${SWLINES_F} | awk -F ';' '{print $6}' )"

		echo "    Copying Swim Lane filter: ${SWLINES_LANE}"

		SQL_SWLINES_INS="insert into AO_60DB71_SWIMLANE(DEFAULT_LANE, DESCRIPTION, NAME, POS, QUERY, LONG_QUERY, RAPID_VIEW_ID) values ('${DEFAULT_LANE}', '${SWLINES_DESCRIPTION}', '${SWLINES_NAME}', ${SWLINES_POS}, '${SWLINES_QUERY}', '${SWLINES_LQUERY}', ${RAPID_DSTID[${ind}]})"

		#echo ${SQL_SWLINES_INS}
		${MYSQL_DST_INS} -e "${SQL_SWLINES_INS}"
		RET=$?
		show_sql_on_error "${SQL_SWLINES_INS}"

	done


done


unset ind


for PKEY in ${RAPID_PRKEY[@]}; do
    if [ -z "$( echo ${PROJECT_KEYS_LIST} | grep ""${PKEY}"" )" ]; then
		if [ -z "${PROJECT_KEYS_LIST}" ]; then
			PROJECT_KEYS_LIST="${PKEY}"
			PROJECT_KEYS="${PKEY}"
		else
			PROJECT_KEYS_LIST="${PROJECT_KEYS_LIST}, ${PKEY}"
			PROJECT_KEYS="${PROJECT_KEYS} ${PKEY}"
		fi
    fi
done

PROJECT_KEYS=$( echo ${PROJECT_KEYS_LIST} | sed -e 's/'\''//g; s/,/ /g' | sed -e 's/ /\n/g' | sort | uniq )

# Copying Custom Fields Data
echo "Copying Custom Fields Data - Other than Sprint one - e.g. Rank, Story Points"

for PKEY in ${PROJECT_KEYS}; do

	for ind in ${!CF_LIST_SRCID[@]}; do

		SQL_CFVALUE="SELECT
    project.pkey
    , jiraissue.issuenum
    , customfieldvalue.CUSTOMFIELD
    , customfieldvalue.PARENTKEY
    , customfieldvalue.STRINGVALUE
    , customfieldvalue.NUMBERVALUE
    , customfieldvalue.TEXTVALUE
    , customfieldvalue.DATEVALUE
    , customfieldvalue.VALUETYPE
FROM
    project
    LEFT JOIN jiraissue 
        ON (project.ID = jiraissue.PROJECT)
    LEFT JOIN customfieldvalue 
        ON (jiraissue.ID = customfieldvalue.ISSUE)
WHERE (project.pkey = '${PKEY}'
    AND customfieldvalue.CUSTOMFIELD = ${CF_LIST_SRCID[${ind}]});"


		${MYSQL_SRC_SEL} -e "${SQL_CFVALUE}" > /dev/null
		RET=$?
		show_sql_on_error "${SQL_CFVALUE}"


		for CFVALUE in $( ${MYSQL_SRC_SEL} -e "${SQL_CFVALUE}" | sed -e 's/ /~~~/g; s/\t/@@@/g'  ); do

			CFVALUE_F=$( echo ${CFVALUE} | sed -e 's/~~~/ /g; s/@@@/;/g' )
			CFVALUE_PRKEY=$( echo ${CFVALUE_F}        | awk -F ';' '{print $1}' )
			CFVALUE_ISSUENUM=$( echo ${CFVALUE_F}     | awk -F ';' '{print $2}' )
			CFVALUE_CUSTOMFIELD=$( echo ${CFVALUE_F}  | awk -F ';' '{print $3}' )
			CFVALUE_PARENTKEY=$( echo ${CFVALUE_F}    | awk -F ';' '{print $4}' )
			CFVALUE_STRINGVALUE="$( echo ${CFVALUE_F} | awk -F ';' '{print $5}' )"
			CFVALUE_NUMBERVALUE="$( echo ${CFVALUE_F} | awk -F ';' '{print $6}' )"
			CFVALUE_TEXTVALUE="$( echo ${CFVALUE_F}   | awk -F ';' '{print $7}' )"
			CFVALUE_DATEVALUE="$( echo ${CFVALUE_F}   | awk -F ';' '{print $8}' )"
			CFVALUE_VALUETYPE="$( echo ${CFVALUE_F}   | awk -F ';' '{print $9}' )"

			SQL_CFVALUE_ID="select max(id)+1 from customfieldvalue"
			CFVALUE_ID=$( ${MYSQL_DST_SEL} -e "${SQL_CFVALUE_ID}" )

			echo "    Copying custom Field Value for Issue ${CFVALUE_PRKEY}-${CFVALUE_ISSUENUM}, CF:${CF_LIST_NAME[${ind}]}"

			SQL_CFVALUE_INS="insert into customfieldvalue(ID, ISSUE, CUSTOMFIELD, STRINGVALUE, NUMBERVALUE, TEXTVALUE, DATEVALUE, VALUETYPE) select ${CFVALUE_ID} as 'ID', jiraissue.id as 'ISSUE', ${CF_LIST_DSTID[${ind}]} as 'CUSTOMFIELD', ${CFVALUE_STRINGVALUE} as 'STRINGVALUE', ${CFVALUE_NUMBERVALUE} as 'NUMBERVALUE', ${CFVALUE_TEXTVALUE} as 'TEXTVALUE', ${CFVALUE_DATEVALUE} as 'DATEVALUE', ${CFVALUE_VALUETYPE} as 'VALUETYPE' from jiraissue INNER JOIN project ON (jiraissue.PROJECT = project.ID) WHERE project.pkey = '${CFVALUE_PRKEY}' AND jiraissue.issuenum = ${CFVALUE_ISSUENUM}"

			${MYSQL_DST_INS} -e "${SQL_CFVALUE_INS}"
			RET=$?
			show_sql_on_error "${SQL_CFVALUE_INS}"

		done

	done

done

# Updating cutom field max values ID
echo "Updating cutom field max values ID"
SQL_CFVALUE_ID_UPD="UPDATE SEQUENCE_VALUE_ITEM SET seq_id = (SELECT MAX(id)+100 FROM customfieldvalue) WHERE seq_name = 'CustomFieldValue';"
${MYSQL_DST_INS} -e "${SQL_CFVALUE_ID_UPD}"
RET=$?
show_sql_on_error "${SQL_CCOLOUR_INS}"


# Fixing ChangeItem Sprint data

SQL_CHITEM_LIST="SELECT
    project.pkey
    , jiraissue.issuenum
    , changegroup.AUTHOR
    , changegroup.CREATED
    , changeitem.ID
    , changeitem.FIELDTYPE
    , changeitem.FIELD
    , changeitem.OLDVALUE
    , changeitem.OLDSTRING
    , changeitem.NEWVALUE
    , changeitem.NEWSTRING
FROM
    project
    LEFT JOIN jiraissue 
        ON (project.ID = jiraissue.PROJECT)
    LEFT JOIN changegroup 
        ON (jiraissue.ID = changegroup.issueid)
    LEFT JOIN changeitem 
        ON (changegroup.ID = changeitem.groupid)
WHERE (project.pkey in ( ${PROJECT_KEYS_LIST} )
    AND changeitem.FIELDTYPE ='custom'
    AND changeitem.FIELD ='Sprint');"

${MYSQL_DST_SEL} -e "${SQL_CHITEM_LIST}" > /dev/null
RET=$?
show_sql_on_error "${SQL_CHITEM_LIST}"

for CHI in $( ${MYSQL_DST_SEL} -e "${SQL_CHITEM_LIST}" | sed -e 's/ /~~~/g; s/\t/@@@/g' ); do

	CHI_F=$( echo ${CHI} | sed -e 's/~~~/ /g; s/@@@/;/g' )
	CHI_ID=$( echo ${CHI_F} | awk -F ';' '{print $5}' )
	CHI_PRKEY=$( echo ${CHI_F} | awk -F ';' '{print $1}' )
	CHI_ISSUENUM=$( echo ${CHI_F} | awk -F ';' '{print $2}' )
	CHI_OLDVALUE=$( echo ${CHI_F} | awk -F ';' '{print $8}'  | sed -f ${SED_SPRINT_ID} )
	CHI_NEWVALUE=$( echo ${CHI_F} | awk -F ';' '{print $10}' | sed -f ${SED_SPRINT_ID} )

	echo "Fixing Change Item ${CHI_ID} for Issue ${CHI_PRKEY}-${CHI_ISSUENUM}"

	SQL_CHI_FIX="update changeitem set OLDVALUE = '${CHI_OLDVALUE}', NEWVALUE = '${CHI_NEWVALUE}' where ID = ${CHI_ID}"

	${MYSQL_DST_INS} -e "${SQL_CHI_FIX}"
	RET=$?
	show_sql_on_error "${SQL_CHI_FIX}"

done



# Updating LexoRank values

# Cleaning up old RANK data if so

echo "Cleaning Up old Lexo Rank values"

SQL_RANK_CLEAN="DELETE 
FROM AO_60DB71_LEXORANK
WHERE ISSUE_ID IN
(
SELECT jiraissue.ID
FROM jiraissue
INNER JOIN project
   ON (jiraissue.PROJECT = project.ID)
WHERE (project.pkey IN (${PROJECT_KEYS_LIST}))
);"

${MYSQL_DST_INS} -e "${SQL_RANK_CLEAN}"

# Populating Lexorank values

SQL_LEXORANK="SELECT
    project.pkey
    , jiraissue.issuenum
    , AO_60DB71_LEXORANK.FIELD_ID
    , AO_60DB71_LEXORANK.ISSUE_ID
    , AO_60DB71_LEXORANK.LOCK_HASH
    , AO_60DB71_LEXORANK.LOCK_TIME
    , AO_60DB71_LEXORANK.RANK
    , AO_60DB71_LEXORANK.TYPE
FROM
    AO_60DB71_LEXORANK
    INNER JOIN jiraissue 
        ON (AO_60DB71_LEXORANK.ISSUE_ID = jiraissue.ID)
    INNER JOIN project
	ON (jiraissue.PROJECT = project.ID)
WHERE (project.pkey IN (${PROJECT_KEYS_LIST}));"

${MYSQL_SRC_SEL} -e "${SQL_LEXORANK}" > /dev/null
RET=$?
show_sql_on_error "${SQL_LEXORANK}"

for RANK in $( ${MYSQL_SRC_SEL} -e "${SQL_LEXORANK}" | sed -e 's/ /@@@/g; s/\t/@@@/g' ); do

	RANK_F=$( echo ${RANK} | sed -e 's/@@@/ /g' )

	RANK_PRKEY=$( echo ${RANK_F} | awk '{print $1}' )
	RANK_ISSUENUM=$( echo ${RANK_F} | awk '{print $2}' )
	RANK_FIELD_ID=$( echo ${RANK_F} | awk '{print $3}' )
	RANK_ISSUE_ID=$( echo ${RANK_F} | awk '{print $4}' )
	RANK_LOCK_HASH=$( echo ${RANK_F} | awk '{print $5}' )
	RANK_LOCK_TIME=$( echo ${RANK_F} | awk '{print $6}' )
	RANK_RANK="\"$( echo ${RANK_F} | awk '{print $7}' )a\""
	RANK_TYPE=$( echo ${RANK_F} | awk '{print $8}' )

	echo "Copying LexoRank for Issue ${RANK_PRKEY}-${RANK_ISSUENUM} (RANK ${RANK_RANK})"

	SQL_RANK_INS="insert into AO_60DB71_LEXORANK(FIELD_ID, ISSUE_ID, LOCK_HASH, LOCK_TIME, RANK, TYPE) select ${CF_RANK_DSTID} as 'FIELD_ID', jiraissue.ID as 'ISSUE_ID', ${RANK_LOCK_HASH} as 'LOCK_HASH', ${RANK_LOCK_TIME} as 'LOCK_TIME', ${RANK_RANK} as 'RANK', ${RANK_TYPE} as 'TYPE' FROM jiraissue INNER JOIN project ON (jiraissue.PROJECT = project.ID) WHERE project.pkey = '${RANK_PRKEY}' AND jiraissue.issuenum = ${RANK_ISSUENUM}"

	#echo "${SQL_RANK_INS}"
	${MYSQL_DST_INS} -e "${SQL_RANK_INS}"
	RET=$?
	show_sql_on_error "${SQL_LEXORANK}"

done

# fixing Statuses

# ToDo - put as parameters ....
# This may be not needed at all if Statuses are Ok

echo "Fixing Statuses - this can take a while."

for ind in ${!STATUS_NAME[@]}; do

	echo -n "    Status: ${STATUS_NAME[${ind}]} - "

	SQL_STATUS_FIX="UPDATE changeitem
SET oldvalue = ${STATUS_NEWID[${ind}]}
WHERE fieldtype = 'jira'
  AND FIELD = 'status'
  AND oldvalue = ${STATUS_OLDID[${ind}]}
  AND oldstring LIKE '${STATUS_NAME[${ind}]}';

UPDATE changeitem
SET newvalue = ${STATUS_NEWID[${ind}]}
WHERE fieldtype = 'jira'
  AND FIELD = 'status'
  AND newvalue = ${STATUS_OLDID[${ind}]}
  AND newstring LIKE '${STATUS_NAME[${ind}]}';"

	${MYSQL_DST_INS} -e "${SQL_STATUS_FIX}"
	RET=$?
	show_sql_on_error "${SQL_STATUS_FIX}"

	echo "Done."

done

echo "Migration Complete !!! Congradulations !!!"


